# nginx-rtmp

[![pipeline status](https://gitlab.com/karliskavacis/nginx-rtmp/badges/master/pipeline.svg)](https://gitlab.com/karliskavacis/nginx-rtmp/commits/master)


Prebuilt NGINX+NGINX-RTMP module for CentOS7

NGINX from https://nginx.org Mainline and Stable sources for CentOS7 x86_64
NGINX-RTMP module from Roman Arutyunyan: https://github.com/arut/nginx-rtmp-module

WHAT WORKS:
CentOS 7 Stable and Mainline (builds and runs)
Ubuntu 18.04, 18.10, 19.04, 19.10 Stable and Mainline (builds and runs)

TODO (planned):
Builds for RHEL (7 & 8), CentOS (7 & 8), Fedora (28, 29, 30 & 31), Debian (7, 8 & 9), Ubuntu (18.04, 18.10, 19.04 & 19.10);
Sample RTMP config included in standard config;

TODO (wishlist):
Dynamic module builds;
Builds for MacOSX, Windows;
Builds for testing releases (i.e. Fedora Rawhide)
Docker images;
Google NGX-PageSpeed module integration;
